Store user password in session
==============================

Presentation
------------

Password is not a common attribute. Indeed, in most of the cases, it is
not stored in clear text in the backend (LDAP or database).

So to keep user password in session, you cannot just export the
password variable in session. To bypass this, LL::NG can remember what
password was given by user during authentication.

.. attention::

    -  As this may be a security hole, password store in session is not
       activated by default
    -  This mechanism can only work with authentication backends using a
       login/password form (:doc:`LDAP<authldap>`, :doc:`DBI<authdbi>`, ...)
       so not with Kerberos for example
    -  This mechanism will not work with Persistent connections plugin, as
       the password is not available if user is automatically connected
    -  Password can be encrypted in session, you need to enable this option,
       as this is disabled by default


Configuration
-------------

Go in Manager, ``General Parameters`` » ``Sessions`` »
``Store user password in session data`` and set to ``On``.

To encrypt value in session, go to ``General Parameters`` » ``Sessions`` »
``Encrypt password in sessio`` and set to ``On``.

Usage
-----

User password is now available in ``$_password`` variable. For example,
to send it in an header:

::

   Auth-Password => $_password


.. tip::

    For security reasons, the password is not shown in sessions
    explorer.
