Plugins
=======

.. toctree::
   :maxdepth: 1

   adaptativeauthenticationlevel
   autosignin
   bruteforceprotection
   cda
   checkdevops
   checkentropy
   checkhibp
   checkstate
   checkuser
   contextswitching
   crowdsec
   decryptvalue
   finduser
   forcereauthn
   globallogout
   grantsession
   impersonation
   initializePasswordReset
   locationdetect
   loginhistory
   notifications
   public_pages
   refreshsessionapi
   rememberauthchoice
   resetpassword
   resetcertificate
   restauthuserpwdbackend
   restservices
   soapservices
   status
   trustedbrowser
   viewer
